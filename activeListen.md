### Question 1

The steps/strategies to do Active Listening are:

1. Paying attention to the speaker and maintaining eye contact.
2. Not interrupting the speaker while they are talking.
3. Using nonverbal cues like nodding, smiling, and leaning forward to show that you are listening and engaged.
4. Asking clarifying questions to ensure that you understand the speaker's message.
5. Paraphrasing what the speaker said to confirm that you have understood them correctly.
6. Providing feedback to the speaker, acknowledging their feelings, and expressing empathy.

### Question 2

According to Fisher's model, the key points of Reflective Listening are:

1. Repeating or paraphrasing the speaker's message to ensure that you have understood it correctly.
2. Reflecting the speaker's emotions and body language to show that you are empathizing with them.
3. Encouraging the speaker to continue talking and expressing themselves.
4. Providing feedback to the speaker and acknowledging their feelings.

### Question 3

The obstacles in my listening process may include:

1. Distractions like noise or visual disturbances.
2. Preconceived notions or biases that may affect my ability to understand the speaker's message.
3. Lack of interest in the topic or the speaker.
4. Feeling defensive or emotionally charged, which may make it difficult to remain calm and attentive.

### Question 4

To improve my listening, I can:

1. Avoid multitasking and distractions while listening.
2. Try to put myself in the speaker's shoes and empathize with them.
3. Avoid interrupting the speaker and wait for them to finish speaking before responding.
4. Clarify any doubts I have by asking questions.
5. Focus on the speaker's message rather than my own opinions or biases.

### Question 5

Passive communication style may be appropriate in situations where I want to avoid conflict or when I do not have a strong opinion on the topic. For example, if someone is discussing a topic that I do not have much knowledge about, I may prefer to listen passively without giving my opinion.

### Question 6

Aggressive communication style may be appropriate when I feel strongly about a topic or when I need to assert my authority. For example, if someone is disrespecting me or my work, I may switch to an aggressive communication style to defend myself.

### Question 7

Passive Aggressive communication style may be appropriate when I want to express my dissatisfaction without being confrontational. For example, if someone is repeatedly ignoring my emails, I may use sarcasm or give them the silent treatment to convey my frustration.

### Question 8

To make my communication assertive, I can follow the steps mentioned in the Tips on Assertive Communication video:

1. Use "I" statements to express my feelings and opinions without blaming others.
2. State my needs clearly and assertively.
3. Use a firm and confident tone of voice.
4. Use nonverbal cues like maintaining eye contact and standing up straight to show confidence.
5. Listen actively and respond respectfully to others' opinions.
