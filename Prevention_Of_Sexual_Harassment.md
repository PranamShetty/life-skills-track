## What kinds of behavior cause sexual harassment?

1. **Verbal Harassment**: This includes unwelcome comments, jokes, sexual innuendos, or persistent requests for dates or sexual favors.
2. **Physical Harassment**: Unwanted physical contact, such as touching, groping, or lewd gestures, constitutes physical sexual harassment.
3. **Non-Verbal Harassment**: Non-verbal behavior like staring, leering, or displaying sexually suggestive images or objects can also be forms of sexual harassment.
4. **Visual Harassment**: Sharing or displaying explicit or offensive images, videos, or messages electronically or in person can create a hostile environment.



## What should you do if you face or witness incidents of sexual harassment?

If you face or witness incidents of sexual harassment, it's crucial to take appropriate action. Here are some steps you can consider:

1. **Ensure personal safety**: If you feel threatened or unsafe, remove yourself from the situation immediately and find a secure space.
2. **Document the incidents**: Keep a record of the incidents, including dates, times, locations, and any witnesses. This documentation can be helpful for future reference or if you decide to report the harassment.
3. **Seek support**: Reach out to someone you trust, such as a friend, family member, or colleague, and share your experience. Their support can provide emotional reassurance and guidance.
4. **Know your rights and policies**: Familiarize yourself with your organization's policies on sexual harassment and the available reporting mechanisms. Understand your rights and the steps you can take.
5. **Report the incidents**: Depending on the severity and your comfort level, report the incidents to the appropriate authorities within your organization. This may include supervisors, human resources, or designated complaint mechanisms.
6. **Gather evidence**: If possible, gather any evidence that supports your case, such as emails, text messages, or witnesses who can corroborate your account.
